const express = require("express");
const fileDb = require("../fileDb");
const router = express.Router();


router.get("/", (req, res) => {
  if (req.query.datetime) {
    const targetDate = new Date(req.query.datetime);

    if (isNaN(targetDate.getDate())) {
      return res.status(400).send({error: "Invalid date provided to server"});
    }

    const messages = fileDb.getItemsFromDate(req.query.datetime);

    res.send(messages);
  } else {
    const messages = fileDb.getItems();

    res.send(messages);
  }
});

router.post("/", (req, res) => {
  if (req.body.message === "" || req.body.author === "") {
    return res.status(400).send({error: "Author or message can not be empty"});
  }

  fileDb.addItem(req.body);
  res.send(req.body);
});

module.exports = router;