const fs = require("fs");
const {nanoid} = require("nanoid");

const filename = "./messagesDb.json";

let messagesData = [];


module.exports = {
  init() {
    try {
      const messagesDbContents = fs.readFileSync(filename);
      messagesData = JSON.parse(messagesDbContents);
    } catch (e) {
      messagesData = [];
    }
  },
  getItems() {
    let responseMessages;
    if (messagesData.length > 30) {
      responseMessages = messagesData.slice(-30);
    } else {
      responseMessages = messagesData;
    }

    return responseMessages;
  },
  getItemsFromDate(date) {
    let responseMessages;
    const targetIndex = messagesData.findIndex(arrItem => arrItem.datetime === date);
    responseMessages = messagesData.slice(targetIndex + 1);

    return responseMessages;
  },
  addItem(message) {
    message.id = nanoid();
    message.datetime = new Date().toISOString();
    messagesData.push(message);
    this.save();
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(messagesData, null, 2));
  }
}