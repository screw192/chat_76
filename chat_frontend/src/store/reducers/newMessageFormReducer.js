import {INPUT_CHANGE, POST_MESSAGE_FAILURE, POST_MESSAGE_SUCCESS} from "../actions/newMessageFormActions";

const initialState = {
  message: "",
  author: "",
  error: false
};

const newMessageFormReducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_CHANGE:
      return {...state, [action.event.target.name]: action.event.target.value};
    case POST_MESSAGE_SUCCESS:
      return {...state, message: "", error: false};
    case POST_MESSAGE_FAILURE:
      return {...state, error: true};
    default:
      return state;
  }
};

export default newMessageFormReducer;