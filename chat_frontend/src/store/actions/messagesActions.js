import axiosChat from "../../axiosChat";

export const FETCH_MESSAGES_REQUEST = "FETCH_MESSAGES_REQUEST";
export const FETCH_MESSAGES_SUCCESS = "FETCH_MESSAGES_SUCCESS";
export const FETCH_MESSAGES_FAILURE = "FETCH_MESSAGES_FAILURE";

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messagesData => ({type: FETCH_MESSAGES_SUCCESS, messagesData});
export const fetchMessagesFailure = error => ({type: FETCH_MESSAGES_FAILURE, error});

export const fetchMessages = () => {
  return async dispatch => {
    try {
      dispatch(fetchMessagesRequest());

      const response = await axiosChat.get("/messages");
      dispatch(fetchMessagesSuccess(response.data));
    } catch (e) {
      dispatch(fetchMessagesFailure(e));
    }
  };
};

export const FETCH_NEW_MESSAGES_REQUEST = "FETCH_NEW_MESSAGES_REQUEST";
export const FETCH_NEW_MESSAGES_SUCCESS = "FETCH_NEW_MESSAGES_SUCCESS";
export const FETCH_NEW_MESSAGES_FAILURE = "FETCH_NEW_MESSAGES_FAILURE";

export const fetchNewMessagesRequest = () => ({type: FETCH_NEW_MESSAGES_REQUEST});
export const fetchNewMessagesSuccess = messagesData => ({type: FETCH_NEW_MESSAGES_SUCCESS, messagesData});
export const fetchNewMessagesFailure = error => ({type: FETCH_NEW_MESSAGES_FAILURE, error});

export const fetchNewMessages = datetime => {
  return async dispatch => {
    try {
      dispatch(fetchNewMessagesRequest());

      const response = await axiosChat.get(`/messages?datetime=${datetime}`);
      dispatch(fetchNewMessagesSuccess(response.data));
    } catch (e) {
      dispatch(fetchNewMessagesFailure(e));
    }
  };
};