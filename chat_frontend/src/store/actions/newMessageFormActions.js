import axiosChat from "../../axiosChat";

export const INPUT_CHANGE = "INPUT_CHANGE";

export const POST_MESSAGE_REQUEST = "POST_MESSAGE_REQUEST";
export const POST_MESSAGE_SUCCESS = "POST_MESSAGE_SUCCESS";
export const POST_MESSAGE_FAILURE = "POST_MESSAGE_FAILURE";

export const inputChange = (event) => ({type: INPUT_CHANGE, event});

export const postMessageRequest = () => ({type: POST_MESSAGE_REQUEST});
export const postMessageSuccess = () => ({type: POST_MESSAGE_SUCCESS});
export const postMessageFailure = (error) => ({type: POST_MESSAGE_FAILURE, error});

export const postMessage = message => {
  return async dispatch => {
    try {
      dispatch(postMessageRequest());

      await axiosChat.post("/messages", message);
      dispatch(postMessageSuccess());
    } catch (e) {
      dispatch(postMessageFailure(e));
    }
  };
};