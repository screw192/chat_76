import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(theme => ({
  staticToolbar: {
    marginBottom: theme.spacing(1)
  }
}));


const AppToolbar = () => {
  const classes = useStyles();

  return (
      <>
        <AppBar position="fixed">
          <Toolbar variant="dense">
            <Typography variant="h6" color="inherit">
              Le Chatique
            </Typography>
          </Toolbar>
        </AppBar>
        <Toolbar className={classes.staticToolbar}/>
      </>
  );
};

export default AppToolbar;