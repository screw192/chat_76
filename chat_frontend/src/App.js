import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";

import Chat from "./containers/Chat/Chat";
import NewMessageForm from "./containers/NewMessageForm/NewMessageForm";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";


const App = () => (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="md">
          <Chat/>
          <NewMessageForm/>
        </Container>
      </main>
    </>
);

export default App;