import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";

import {inputChange, postMessage} from "../../store/actions/newMessageFormActions";


const useStyles = makeStyles((theme) => ({
  blockMargin: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4),
  },
  inputMargin: {
    marginBottom: theme.spacing(1),
  },
  margin: {
    height: "100%",
    marginLeft: theme.spacing(2),
  }
}));


const NewMessageForm = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const authorInput = useSelector(state => state.messageForm.author);
  const messageInput = useSelector(state => state.messageForm.message);

  const sendMessageHandler = e => {
    e.preventDefault();

    dispatch(postMessage({
      message: messageInput,
      author: authorInput
    }));
  };

  return (
      <form onSubmit={sendMessageHandler}>
        <Grid container spacing={2} className={classes.blockMargin}>
          <Grid item container>
            <Box flexGrow={1}>
              <TextField
                  className={classes.inputMargin}
                  label="Author"
                  id="authorNameInput"
                  name="author"
                  fullWidth
                  value={authorInput}
                  variant="outlined"
                  size="small"
                  onChange={(event) => dispatch(inputChange(event))}
              />
              <TextField
                  label="Message"
                  id="messageInput"
                  name="message"
                  fullWidth
                  value={messageInput}
                  variant="outlined"
                  size="small"
                  onChange={(event) => dispatch(inputChange(event))}
              />
            </Box>
            <Button
                type="submit"
                variant="contained"
                size="large"
                color="primary"
                className={classes.margin}
            >
              Send
            </Button>
          </Grid>
        </Grid>
      </form>
  );
};

export default NewMessageForm;