import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";

import {fetchMessages, fetchNewMessages} from "../../store/actions/messagesActions";
import ChatMessage from "./ChatMessage/ChatMessage";


const Chat = () => {
  const dispatch = useDispatch();
  const messagesData = useSelector(state => state.messages.messages);
  const lastMsgDate = useSelector(state => state.messages.lastMessageDatetime);

  useEffect(() => {
    dispatch(fetchMessages());
  }, [dispatch]);

  useEffect(() => {
    let interval;

    if (lastMsgDate) {
      interval = setInterval(() => {
        dispatch(fetchNewMessages(lastMsgDate));
      }, 5000);
    }
    return () => clearInterval(interval);
  }, [dispatch, lastMsgDate]);

  const chatMessages = messagesData.map(message => {
    return (
      <ChatMessage
          key={message.id}
          author={message.author}
          message={message.message}
          date={message.datetime}
      />
    );
  });

  return (
      <Grid container direction="column" spacing={2}>
        {chatMessages}
      </Grid>
  );
};

export default Chat;