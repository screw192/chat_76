import React from 'react';
import moment from "moment";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  border: {
    borderBottom: "1px solid #eeeeee"
  }
}));


const ChatMessage = ({author, message, date}) => {
  const classes = useStyles();
  const msgDate = moment(date).calendar();

  return (
      <Grid item>
        <Card>
          <CardHeader
              className={classes.border}
              title={author}
              subheader={msgDate}
          />
          <CardContent>
            <Typography variant="body1">
              {message}
            </Typography>
          </CardContent>
        </Card>
      </Grid>
  );
};

export default ChatMessage;